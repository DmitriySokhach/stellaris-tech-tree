from deep_parsers.feature_unlocks import FeatureUnlocks
from deep_parsers.weight_modifiers import parse as parse_weight_modifiers
from json import JSONEncoder


class Technology:
    def __init__(self, tech, armies, army_attachments, buildable_pops,
                 buildings, components, edicts, policies, resources,
                 starport_buildings, starport_modules, tile_blockers, loc_data,
                 at_vars, start_with_tier_zero=True):
        self.key = tech.keys()[0]

        print(self.key)

        self._at_vars = at_vars
        self._loc_data = loc_data
        self.name = loc_data.get(self.key, self.key)

        tech_data = tech[self.key]
        tech_dict = {}
        for item in tech_data:
            _keyName =item.keys()[0]
            if _keyName in tech_dict.keys():
                if(not _keyName.startswith('is')):
                    raise KeyError("Duplicate Key in data", _keyName)
            else:
                tech_dict[_keyName] = item.values()[0]

        self.description = self._description()
        self.area = tech_dict['area']
        self.category = loc_data[
            tech_dict['category'][0]
        ]

        self.tier = tech_dict['tier']
        if type(self.tier) is not int and self.tier.startswith('@'):
            self.tier = self._at_vars[self.tier]

        self.cost = self._cost(tech_dict)
        self.base_weight = self._base_weight(tech_dict)
        self.base_factor = self._base_factor(tech_dict)
        self.weight_modifiers = self._weight_modifiers(tech_dict)
        self.prerequisites = self._prerequisites(tech_dict)
        self.is_start_tech = self._is_start_tech(tech_dict,
                                                 start_with_tier_zero)
        self.is_dangerous = self._optionalBoolean('is_dangerous', tech_dict)
        self.is_rare = self._optionalBoolean('is_rare',tech_dict)

        self.icon = self._icon(tech_dict)

        unlock_parser = FeatureUnlocks(armies, army_attachments,
                                       buildable_pops, buildings, components,
                                       edicts, policies, resources,
                                       starport_buildings, starport_modules,
                                       tile_blockers, loc_data)
        self.feature_unlocks = unlock_parser.parse(self.key, tech_data)

    def _is_start_tech(self, tech_dict, start_with_tier_zero):
        if 'start_tech' in tech_dict.keys():
            yes_no = tech_dict['start_tech']
            # print(yes_no)
            is_start_tech = True if yes_no == 'yes' else False
        else:
            # print('DEBUG -- Is Start Tech -- StopIteration exception')
            is_start_tech = True if self.tier == 0 and start_with_tier_zero \
                            else False

        return is_start_tech

    def _optionalBoolean(self, keyName, tech_dict):
        if keyName in tech_dict.keys():
            yes_no = tech_dict[keyName]
            booleanVal = True if yes_no == 'yes' else False
        else:
            booleanVal = False

        return booleanVal

    def _description(self):
        try:
            description = self._loc_data[self.key + '_desc']
            description = (self._loc_data[description.replace('$', '')]
                                if description.startswith('$')
                                else description)
        except KeyError:
            description = None

        return description

    def _prerequisites(self, tech_dict):
        if self.key in ['tech_biolab_1', 'tech_physics_lab_1',
                        'tech_engineering_lab_1']:
            prerequisites = ['tech_basic_science_lab_1']
        elif 'prerequisites' in tech_dict.keys():
            prerequisites = tech_dict['prerequisites']
        else:
            prerequisites = []

        return prerequisites

    def _cost(self, tech_dict):
        # print("DEBUG tech_data cost")
        # print(tech_dict)
        if 'cost' in tech_dict.keys():
            string = tech_dict['cost']
            return self._at_vars[string] if str(string).startswith('@') else string
        else:
            return "0"

    def _base_weight(self, tech_dict):
        if 'weight' in tech_dict.keys():
            string = tech_dict['weight']
            weight = (self._at_vars[string]
                      if str(string).startswith('@')
                      else string)
        else:
            weight = 0

        return weight

    def _base_factor(self, tech_dict):
        try:
            string = tech_dict['weight_modifier'][0]['factor']
            factor = (self._at_vars[string]
                      if str(string).startswith('@')
                      else string)
        except (StopIteration, KeyError, IndexError):
            factor = 1.0

        return float(factor)

    def _weight_modifiers(self, tech_dict):
        if 'potential' in tech_dict.keys():
            potential = tech_dict['potential']
            potential_modifiers = [ {'modifier':[{'factor':0},{'NOR':potential}]} ]
        else:
            potential_modifiers = []

        if 'weight_modifier' in tech_dict.keys():
            weight_modifiers = tech_dict['weight_modifier']
        else:
            weight_modifiers = []

        unparsed_modifiers = potential_modifiers + weight_modifiers

        return [parse_weight_modifiers(modifier['modifier'], self._loc_data)
                for modifier in unparsed_modifiers
                if modifier.keys() == ['modifier']]

    def _icon(self, tech_dict):
        if 'icon' in tech_dict.keys():
            icon = tech_dict['icon']

            # deal with human error in data
            if icon.startswith("t_"):
                icon = self.key
        else:
            icon = self.key

        return icon



class TechnologyJSONEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, Technology):
            encoder = {key: getattr(object, key) for key
                       in object.__dict__.keys()
                       if not key.startswith('_')}
        else:
            encoder = JSONEncoder.default(self, object)

        return encoder
