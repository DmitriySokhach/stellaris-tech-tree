# -*- coding: utf-8 -*-

from pprint import pprint
import re

nested_key_re = r'\$(\w+)\$'

class FeatureUnlocks:
    def __init__(self, armies, army_attachments, buildable_pops, buildings,
                 components, edicts, policies, resources, starport_buildings,
                 starport_modules, tile_blockers, loc_data):
        self._armies = armies
        self._army_attachments = army_attachments
        self._buildable_pops = buildable_pops
        self._buildings = buildings
        self._components = components
        self._edicts = edicts
        self._policies = policies
        self._resources = resources
        self._starport_buildings = starport_buildings
        self._starport_modules = starport_modules
        self._tile_blockers = tile_blockers
        self._loc_data = loc_data

        def _localize(string):
            localized = loc_data[string] if type(string) is str \
                        else loc_data[string.group(1)]

            while '$' in localized:
                localized = re.sub(nested_key_re, _localize, localized)

            return localized

        self._localize = _localize

    # Modifiers gained as a result of completing research
    def parse(self, tech_key, tech_data):
        feature_flags = self._feature_flags(tech_data)
        custom_unlock_tooltip = self._unlocks(tech_data)
        unlocks = self._modifiers(tech_data) \
                  + custom_unlock_tooltip + feature_flags \
                  + ([] if custom_unlock_tooltip != []
                     else self._army_unlocks(tech_key)
                     + self._army_attachment_unlocks(tech_key)
                     + self._buildable_pop_unlocks(tech_key)
                     + self._building_unlocks(tech_key)
                     + feature_flags
                     + self._component_unlocks(tech_key)
                     + self._edict_unlocks(tech_key)
                     + self._policy_unlocks(tech_key)
                     + self._resource_unlocks(tech_key)
                     + self._starport_module_unlocks(tech_key)
                     + self._starport_building_unlocks(tech_key)
                     + self._tile_blocker_unlocks(tech_key))

        return unlocks

    def _modifiers(self, tech_data):

        def localize(modifier):
            key = modifier.keys()[0]
            # pprint(key)
            # pprint(modifier)
            if key in ('description_parameters',
                       'BIOLOGICAL_species_trait_points_add',
                       'show_only_custom_tooltip'
                       ):
                return None

            if key in ('custom_tooltip'):
                key = modifier[key]
                localized = self._localize(key)
                return localized
                # print("Non standard UNLOCK: {}".format(key))
            elif key in ('description'):
                localized = self._loc_data[modifier[key]]

                # TODO reduce reduntant implementations
                # deal with nested localizations
                if localized.find('$') != -1:
                    vars = re.findall(nested_key_re,localized)
                    loc = []
                    for v in vars:
                        t = v.replace('$', '')
                        try:
                            t = self._loc_data[t]
                        except KeyError:
                            print('Could NOT find {} key'.format(v))
                            t = v

                        loc.append(t)

                    localized = re.sub(nested_key_re,'{}', localized)
                    localized = localized.format(*loc)
                return localized
            else:
                # print("_Modifiers -- KEY: {}\nVALUE: {}".format(key, modifier[key]))
                value = ('{:+.0%}'.format(modifier[key])
                         if modifier[key] < 1
                         or int(modifier[key]) != modifier[key]
                         else '{:+d}'.format(int(modifier[key])))

            if key == 'all_technology_research_speed':
                key = 'MOD_COUNTRY_ALL_TECH_RESEARCH_SPEED'
            elif key == 'science_ship_survey_speed':
                key = 'MOD_SHIP_SCIENCE_SURVEY_SPEED'

            try:
                localized = {self._localize(key): value}
            except KeyError:
                prefix = 'MOD_'
                alt_key = (prefix + key).upper()
                try:
                    localized_key = self._localize(alt_key)
                    localized = {localized_key: value}
                    while '$' in localized_key:
                        localized_key = re.sub(r'\$(\w+)\$',
                                               self._localize,
                                               localized_key)
                        localized = {localized_key: value}

                except KeyError:
                    prefix = 'MOD_COUNTRY_'
                    alt_key = (prefix + key).upper()
                    try:
                        localized = {self._localize(alt_key): value}
                    except KeyError:
                        prefix = 'MOD_POP_'
                        alt_key = (prefix + key).upper()
                        try:
                            localized = {self._localize(alt_key): value}
                        except KeyError:
                            prefix = 'MOD_PLANET_'
                            alt_key = (prefix + key).upper()
                            try:
                                localized = {self._localize(alt_key): value}
                            except KeyError:
                                # Give up.
                                localized = {key: value}

            return u'{}: {}'.format(localized.keys()[0], localized.values()[0])

        try:
            acquired_modifiers = [localize(modifier)
                                  for modifier
                                  in next(iter(attribute for attribute
                                               in tech_data
                                               if attribute.keys()[0] == 'modifier'))['modifier']]
        except (StopIteration):
            acquired_modifiers = []

        return acquired_modifiers


    def _unlocks(self, tech_data):
        def localize(string):
            try:
                relocalize = lambda match: self._localize(match.group(1))
                localized = re.sub(r'\$(\w+)\$', relocalize,
                                   self._localize(string))
            except KeyError:
                localized = string

            return localized

        try:
            # print(tech_data)
            unlock_types = [unlock_type for unlock_type in next(iter(
                attribute for attribute in tech_data
                if attribute.keys()[0] == 'prereqfor_desc'
            ))['prereqfor_desc']]

            # print("DEBUG -- unlock_types")
            # print(unlock_types)
            # print(type(unlock_types))

            filtered_unlock_types = [keyval for keyval in unlock_types
                    if keyval.keys()[0] not in ['hide_prereq_for_desc']]

            # print("DEBUG -- filtered_unlock_types")
            # print(filtered_unlock_types)
            # print(type(filtered_unlock_types))

            feature_unlocks = [localize(unlock.values()[0][0]['title'])
                               for unlock in filtered_unlock_types]
        except (StopIteration):
            feature_unlocks = []

        return feature_unlocks

    def _feature_flags(self, tech_data):
        try:
            feature_flags = [
                'Unlocks Feature: ' + self._localize('feature_' + feature_flag)
                for feature_flag
                in next(iter(
                    attribute for attribute in tech_data
                    if attribute.keys()[0] == 'feature_flags'
                ))['feature_flags']
            ]
        except (StopIteration):
            feature_flags = []

        return feature_flags

    def _army_unlocks(self, tech_key):
        unlocked_armies = [army for army in self._armies
                              if tech_key in army.prerequisites]
        return ['Unlocks Army: {}'.format(army.name)
                for army in unlocked_armies]

    def _army_attachment_unlocks(self, tech_key):
        unlocked_attachments = [attachment for attachment
                                in self._army_attachments
                                if tech_key in attachment.prerequisites]
        return ['Unlocks Attachment: {}'.format(attachment.name)
                for attachment in unlocked_attachments]

    def _buildable_pop_unlocks(self, tech_key):
        unlocked_buildable_pops = [buildable_pop for buildable_pop
                                   in self._buildable_pops
                                   if tech_key in buildable_pop.prerequisites]
        return ['Unlocks Buildable Pop: {}'.format(buildable_pop.name)
                for buildable_pop in unlocked_buildable_pops]

    def _building_unlocks(self, tech_key):
        unlocked_buildings = [building for building in self._buildings
                              if tech_key in building.prerequisites]
        return ['Unlocks Building: {}'.format(building.name)
                for building in unlocked_buildings]

    def _component_unlocks(self, tech_key):
        unlocked_components = [component for component in self._components
                              if tech_key in component.prerequisites]
        return ['Unlocks Component: {}'.format(component.name)
                for component in unlocked_components]

    def _edict_unlocks(self, tech_key):
        unlocked_edicts = [edict for edict in self._edicts
                              if tech_key in edict.prerequisites]
        return ['Unlocks Edict: {}'.format(edict.name)
                for edict in unlocked_edicts]

    def _policy_unlocks(self, tech_key):
        def relevant_options(policy):
            return [{'name': option.name,
                     'policy_name': policy.name }
                    for option in policy.options
                    if tech_key in option.prerequisites]

        unlocked_options = [option for options
                            in [relevant_options(policy)
                                for policy in self._policies]
                            for option in options]

        return ['Unlocks Policy: {} - {}'.format(option['policy_name'],
                                                 option['name'])
                for option in unlocked_options]

    def _resource_unlocks(self, tech_key):
        unlocked_resources = [resource for resource in self._resources
                              if tech_key in resource.prerequisites]
        return ['Can Harvest Resource: {} ([[{}]])'.format(resource.name,
                                                       resource.key)
                for resource in unlocked_resources]

    def _starport_module_unlocks(self, tech_key):
        unlocked_modules = [module for module in self._starport_modules
                            if tech_key in module.prerequisites]
        return ['Unlocks Starport Module: {}'.format(module.name)
                for module in unlocked_modules]

    def _starport_building_unlocks(self, tech_key):
        unlocked_starport_buildings = [module for module in self._starport_buildings
                            if tech_key in module.prerequisites]
        return ['Unlocks Starport Building: {}'.format(module.name)
                for module in unlocked_starport_buildings]

    def _tile_blocker_unlocks(self, tech_key):
        unlocked_blockers = [blocker for blocker in self._tile_blockers
                             if tech_key in blocker.prerequisites]
        return ['Clear Blockers: {}'.format(blocker.name)
                for blocker in unlocked_blockers]
