#!/usr/bin/python

"""
https://docs.microsoft.com/en-us/windows/desktop/direct3ddds/dds-header

C++
typedef struct {
  DWORD           dwSize;
  DWORD           dwFlags;
  DWORD           dwHeight;
  DWORD           dwWidth;
  DWORD           dwPitchOrLinearSize;
  DWORD           dwDepth;
  DWORD           dwMipMapCount;
  DWORD           dwReserved1[11];
  DDS_PIXELFORMAT ddspf;
  DWORD           dwCaps;
  DWORD           dwCaps2;
  DWORD           dwCaps3;
  DWORD           dwCaps4;
  DWORD           dwReserved2;
} DDS_HEADER;

struct DDS_PIXELFORMAT {
  DWORD dwSize;
  DWORD dwFlags;
  DWORD dwFourCC;
  DWORD dwRGBBitCount;
  DWORD dwRBitMask;
  DWORD dwGBitMask;
  DWORD dwBBitMask;
  DWORD dwABitMask;
};
"""

import struct

_ddsId = "DDS "
_ddsHeaderSize = 124

_DDPF_RGB = 0x40
_DDPF_ALPHA = 0x1

def _readToInt32(l):
    return struct.unpack('i', l)[0]

"""
DDS File Header Probe
Determines pixelFormat of provided DDS file
"""
class DDSProbe:
    def readFile(self, fileName):
        try:
            fin = open(fileName)
            formatId = fin.read(4)
            if formatId != _ddsId:
                fin.close()
                raise IOError("Not DDS File Format")

            readHeaderSize = fin.read(4)
            if _readToInt32(readHeaderSize) != _ddsHeaderSize:
                fin.close()
                raise IOError("Invalid DDS Header Size")

            readFlags = fin.read(4)
            readHeight = fin.read(4)
            readWidth = fin.read(4)
            readPitchOrLinearSize = fin.read(4)
            readDepth = fin.read(4)
            readMipMapCount = fin.read(4)

            # 11 reserved dwords
            fin.read(11*4)

            # pixelFormat struct
            readPixelFormatSize = fin.read(4)

            readPixelFormatFlags = fin.read(4)
            self.pixelFormatFlags = _readToInt32(readPixelFormatFlags)
            self.compressionFormat = fin.read(4)

            readRGBBitCount = fin.read(4)
            self.RGBBitCount = _readToInt32(readRGBBitCount)

            readRBitMask = fin.read(4)
            readGBitMask = fin.read(4)
            readBBitMask = fin.read(4)
            readABitMask = fin.read(4)

            # Caps
            readCaps = fin.read(4)
            readCaps2 = fin.read(4)
            readCaps3 = fin.read(4)
            readCaps4 = fin.read(4)

            self.pixelFormatName = self._determinePixelFormatName()

        finally:
            fin.close()


    def _determinePixelFormatName(self):
        pixelFormatName = 'Unknown'
        if self.pixelFormatFlags & _DDPF_RGB == _DDPF_RGB:
            if self.pixelFormatFlags & _DDPF_ALPHA == _DDPF_ALPHA:
                pixelFormatName = 'RGBA'
            else:
                pixelFormatName = 'RGB'

        pixelFormatName = "{}{}".format(pixelFormatName,self.RGBBitCount)
        return pixelFormatName
